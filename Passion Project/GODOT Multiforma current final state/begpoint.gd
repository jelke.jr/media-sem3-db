extends CharacterBody2D

# Rotation variables
var rotation_angle = 0          # Defines the default state of general rotation
var rotation_speed = 3          # Adjust the rotation speed as desired
var rotation_direction = 0      # Defines the default state of rotation direction
const MAX_ROTATION = 75         # Defines the maximal range of rotation

# Jump variables
var jumping = false             # Defines the default state of jumping
var jump_speed = 500            # Defines the speed of the jump animation
var jump_height = 0             # Defines the default state of the height of jump
var jump_distance = 300         # Adjust the jump distance as desired
const GRAVITY = 1000            # Adjust the gravity strength as desired
var can_jump = true             # Tracks if the character can initiate a jump

# Movement variables
var forward_vector = Vector2.ZERO   # Defines the null state of the x and y vector (0,0)
var vertical_velocity = 0           # Defines the default state of vertical_velocity
var dpoint = Vector2.ZERO
var horizontal_speed = 200      # Adjust the horizontal movement speed as desired

enum Rotation_direction {          # Sets default parameters for movement
	STOP = 0,
	RIGHT = 1,
	LEFT = -1
}
func _input(event: InputEvent) -> void:
	# Check if the jump action is pressed and character can jump
	if event.is_action_pressed("jump") and can_jump:
		print("jumped")
		jumping = true
		vertical_velocity = jump_speed
		can_jump = false

	# Check for left and right movement
	if Input.is_action_pressed("left"):
		forward_vector.x = -1
	elif Input.is_action_pressed("right"):
		forward_vector.x = 1
	else:
		forward_vector.x = 0

func _physics_process(delta: float) -> void:
	# Update rotation angle based on rotation speed and direction
	rotation_angle += rotation_speed * rotation_direction
	rotation_angle = clamp(rotation_angle, -MAX_ROTATION, MAX_ROTATION)

	# Update velocity based on gravity and rotation
	velocity = velocity.normalized() * (velocity.length() - GRAVITY * delta)
	velocity = velocity.rotated(rotation_angle)
	print (rotation_angle)
	print (position)
	# Update horizontal movement
	var _horizontal_speed = forward_vector * horizontal_speed
	velocity.x = _horizontal_speed.x

	move_and_slide()

	# Update vertical velocity for jumping
	if jumping:
		vertical_velocity -= GRAVITY * delta

	# Update jump height based on vertical velocity
	jump_height += vertical_velocity * delta

	# Check if character has landed
	if jump_height <= 0:
		jump_height = 0
		vertical_velocity = 0
		jumping = false
		# Reset the can_jump variable when the character lands
		can_jump = true
	

	# Update rotation and vertical position
	self.rotation_degrees = rotation_angle
	self.position.y = -jump_height
	# Update position based on velocity
	dpoint = position               # Store the initial position
	position += velocity * delta    # Update the position using velocity
	print(position)

	if Input.is_action_pressed("rotate_left") == Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.STOP
	elif Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.RIGHT
	elif Input.is_action_pressed("rotate_left"):
		rotation_direction = Rotation_direction.LEFT

