extends Node2D

var hexagon

func _ready():
	hexagon = load("res://path/to/hexagon.tscn")  # Replace "res://path/to/hexagon.tscn" with the correct path

func _on_timer_timeout():
	if hexagon:
		var h = hexagon.instance()
		add_child(h)

