extends Sprite2D

var radius = 100.0
var speed = 100.0
var origin = Vector2(0, -325)

func _process(delta):
	var direction = (origin - position).normalized()
	position += direction * speed * delta
