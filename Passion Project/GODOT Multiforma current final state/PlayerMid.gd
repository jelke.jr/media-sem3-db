extends CharacterBody2D

# Rotation variables
var rotation_angle = 0          # Defines the default state of general rotation
var rotation_speed = 3          # Adjust the rotation speed as desired
var rotation_direction = 0      # Defines the default state of rotation direction
const MAX_ROTATION = 75  

var forward_vector = Vector2.ZERO   # Defines the null state of the x and y vector (0,0)
var vertical_velocity = 0           # Defines the default state of vertical_velocity
var dpoint = Vector2.ZERO
var horizontal_speed = 200      # Adjust the horizontal movement speed as desired

enum Rotation_direction {          # Sets default parameters for movement
	STOP = 0,
	RIGHT = 1,
	LEFT = -1
}
func _input(event: InputEvent) -> void:

	# Check for left and right movement
	if Input.is_action_pressed("left"):
		forward_vector.x = -1
	elif Input.is_action_pressed("right"):
		forward_vector.x = 1
	else:
		forward_vector.x = 0

func _physics_process(delta: float) -> void:
	# Update rotation angle based on rotation speed and direction
	rotation_angle += rotation_speed * rotation_direction
	
	var _horizontal_speed = forward_vector * horizontal_speed
	velocity.x = _horizontal_speed.x

	move_and_slide()

	# Update rotation and vertical position
	self.rotation_degrees = rotation_angle
	# Update position based on velocity
	dpoint = position               # Store the initial position
	position += velocity * delta    # Update the position using velocity
	print(position)

	if Input.is_action_pressed("rotate_left") == Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.STOP
	elif Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.RIGHT
	elif Input.is_action_pressed("rotate_left"):
		rotation_direction = Rotation_direction.LEFT


