let loopBeat;
let bassSynth;
let panner;
let listener;

//creates a synth beat on a loop on pageload
function setup(){
    bassSynth = new Tone.MembraneSynth().toDestination();
    loopBeat = new Tone.Loop(song, '4n');
    Tone.Transport.start();
    loopBeat.start(0);
    //3Dpanner connecting it to the master output
    panner = new Tone.Panner3D(0,0,0).toDestination();
    bassSynth.connect(panner);
    //create listener object and connect it to master output
}

//sound rotation
function rotateSound(){
    const t =Tone.now() + 2;
    panner.positionX.setValueAtTime(Math.sin(t), t);
    panner.positionY.setValueAtTime(Math.cos(t), t);
    panner.positionZ.setValueAtTime(0, t);  
    setTimeout(rotateSound, 2000); //Repeat every 2  second
}


function song(time){
    //trigger the synth sound with  a velocity of 0.5 (volume changer)
    bassSynth.triggerAttackRelease("c1", "8n", time, 0.5)
    console.log(time)
}

setup();
song();
rotateSound();

//set the position of the 3D sound with x,y,z values
