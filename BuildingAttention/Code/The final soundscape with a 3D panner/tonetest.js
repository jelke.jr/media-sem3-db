let sample1, sample2, sample3;
let panner1, panner2, panner3;
let listener

//defines the samples for each sound
function loadSamples() {
  sample1 = new Tone.Player("leaves.mp3").toDestination();
  sample2 = new Tone.Player("sea.mp3").toDestination();
  sample3 = new Tone.Player("bird.mp3").toDestination();

  panner1 = new Tone.Panner3D(1, 0, 0).toDestination();
  panner2 = new Tone.Panner3D(1, 0, 0).toDestination();
  panner3 = new Tone.Panner3D(1, 0, 0).toDestination();
}

function setup() {
  Tone.start();
  loadSamples();
  Tone.Transport.start();

  const loop1 = new Tone.Loop(time => {
    sample1.start();
  }, 8).start(0);

  const loop2 = new Tone.Loop(time => {
    sample2.start();
  }, 8).start(0);

  const loop3 = new Tone.Loop(time => {
    sample3.start();
  }, 8).start(0);

  listener = new Tone.Listener().toDestination();
  // set the position of the listener
  listener.setPosition(0, 0, 0);

  // set the position of the panners
  panner1.positionX.setValueAtTime(5, 0);
  panner2.positionX.setValueAtTime(0, 0);
  panner3.positionX.setValueAtTime(0, 0);


  // connect the players to the panners
  sample1.connect(panner1);
  sample2.connect(panner2);
  sample3.connect(panner3);
}

setup();

// update the positions every frame (every 0.1 seconds)
Tone.AnimationFrame(function(time) {
    // set the position of the panners
    const angle1 = (time / 4) * Math.PI * 2; // rotate once every 4 seconds
    const x1 = Math.sin(angle1) * 5;
    const z1 = Math.cos(angle1) * 5;
    panner1.positionX.setValueAtTime(x1 + 5, time);
    panner1.positionZ.setValueAtTime(z1, time);
  
    const angle2 = (time / 4) * Math.PI * 2; // rotate once every 4 seconds
    const x2 = Math.sin(angle2) * 5;
    const z2 = Math.cos(angle2) * 5;
    panner2.positionX.setValueAtTime(x2, time);
    panner2.positionZ.setValueAtTime(z2, time);
  
    const angle3 = (time / 4) * Math.PI * 2; // rotate once every 4 seconds
    const x3 = Math.sin(angle3) * 5;
    const z3 = Math.cos(angle3) * 5;
    panner3.positionX.setValueAtTime(x3, time);
    panner3.positionZ.setValueAtTime(z3, time);
  
    // set the position of the listener
    listener.setPosition(-x1, 0, -z1);
  }, 0.1);