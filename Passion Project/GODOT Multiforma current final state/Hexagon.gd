extends Node2D

# Variables to control the hexagon size and line width
var hexagon_size = 7
var hexagon_line = 2

# Called when the node is ready to be used
func _ready():
	# Set a random rotation for the node in degrees
	self.rotation_degrees = randi_range(-360, 360)
	# Initialize the random number generator
	randomize()

# Called every frame
func _process(delta):
	# Set the scale of the node based on hexagon_size
	self.scale = Vector2(hexagon_size, hexagon_size)
	# Set the width of the Line2D node (assuming it exists in the scene)
	$Line2D.width = hexagon_line

	# Decrease the hexagon_size by a small amount each frame
	hexagon_size -= 0.03
	# Increase the hexagon_line width if hexagon_size is greater than 2
	if hexagon_size > 2:
		hexagon_line += 0.07
	# Increase the hexagon_line width more if hexagon_size is less than 2
	if hexagon_size < 2:
		hexagon_line += 0.2
	
	# If the hexagon_size becomes negative, remove the node from the scene
	if hexagon_size < 0:
		queue_free()

# Called when another Area2D body enters the node's area
func _on_area_2d_body_entered(body):
	# Check if the colliding body has the name "Player"
	if body.name == "Player":
		# Pause the scene
		get_tree().paused = true
		# Wait for 1 second
		await(get_tree().create_timer(1.0))
		# Resume the scene
		get_tree().paused = false
		# Reload the current scene
		get_tree().reload_current_scene()
	pass

