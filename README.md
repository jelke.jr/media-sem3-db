# Media SEM3-DB

## Project BuildingAttention
Project BuildingAttention was a project, conducted by Jelke van Beekveld, Lars Hoeijmans, Thuvaragan Vanniyasingam and Mohammad Alkhateb. The goal of this project was to create a more subliminally relaxing, yet efficienty increasing open learning space in the Fontys IT buildings.

## Aspects
The project consisted of two large parts, one being blue and green light that was used to verify if they had impact on a students calmness and productivity. My own part considered of creating soundscapes. These were nature themed, 3D atmospheric sound designs that would give the impression of being in nature, while still residing within an open learning space.

## Code used for soundscapes
Within this GitLab repository, multiple code files can be used to utilize for creating soundscapes; However, not all of them are particularly recommended. For instance: Tone.JS was a piece of code I at first tried to use in order to create a soundscape that would pan around ones head; However, as of current, the code only manages to pan the sound form left to right and repeat. 

Soundscape.JS on the other hand, consists of a 3D panner utilized by the official Tone.JS website library, that offers an initial piece of code to create ones own panner and play around with it. 

## Code used for light
During the coding process, I was asked by my colleagues to also prepare a back-up prototype of a device that would be able to manipulate a LED-strip using a website. Because I used an arduino, it wasn't a possibility for me to simply use a web API to get the job done. I had to use a couple of different things such as: Node.js, Johnny-five, WebSocket and Express.js. In the following paragraphs the usage for said code is explained.

## Setting up Tone.JS for using 3D panner soundscapes
Setting up Tone.js isn't particularly difficult. You'll need a source-code editor such as VScode and the primary addon Node.JS which you can download here: https://nodejs.org/en. When you have downloaded and installed Node.JS follow the following steps:

- Open the VScode or the command prompt terminal of your system and type: "NPM install Tone.js". This will include Tone.JS into your package. 
- From then on either utilize the code provided within this repository or go to the official Tone.js website: https://tonejs.github.io. In order to create your own website music.

## Setting up a Neopixel LED-strip system using Nodepixel, Johnny-five, Express and WebSocket.
As stated before, the usage of an arduino for a Neopixel LED strip on a website, can be a little difficult. The most difficulties I had, came with installing node-pixel.JS via Node.JS. This caused frequent serial port errors, which denied me the install. One way to get around this is by utilizing "Yarn". Yarn also gives one the ability to install packages. From then on follow the following steps:

- Open VScode or the command prompt terminal and install: Express, websocket, node-pixel and johnny-five via NPM install commands.
- <Optional>: Should node-pixel not work via NPM install utilize yarn and type in the cmd or vscode terminal: "yarn add node-pixel". This should install the package regardless.
- Utilize the code provided for the Neopixel LED or experiment on your own. 

## Using Godot
In order to be able to use the Godot scripts and files, you will need to import the entire godot folder as a .zip file. Then extract this file into a folder or location of your choice. Following this, you will also need to install the Godot engine. You can do this via this websit: https://godotengine.org.

If this is done, you should be able to import the entire godot folder as one whole project, from then on you are free to experiment around with it. All code is commented.

## Support
If you need any help you can contact me via E-mail: 415320@student.fontys.nl
Should you have any problems regarding the code, visit the pre-mentioned websites or source codes provided within the code themselves.

## Acknowledgment
I want to take a moment and thank both my team members as well as our experts for advising and helping us continuously through this project. It wouldn't be where it is now and we wouldn't have the results that we received as of current.

## Project status
The project is completed thus far; However, there is a lot of room for improvement, especially with the tests. The tests that were conducted, were on a small 1-person per test scale and not in an actual open learning space. This meant that we were able to test the measurability of the product, but not the natural real-time response that it would yield in the actual intended spaces. It is recommend to conduct further research on these aspects.
