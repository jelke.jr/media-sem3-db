extends RigidBody2D

var bullet_velocity = Vector2(1, 0)
var bullet_speed = 300

func _process(delta):
	linear_velocity = bullet_velocity.normalized() * bullet_speed * delta
