extends CharacterBody2D

# Rotation variables
var rotation_angle = 0
var rotation_speed = 3
var rotation_direction = 0

# Movement variables
var forward_vector = Vector2.ZERO
var vertical_velocity = 0
var dpoint = Vector2.ZERO
var horizontal_speed = 200

# Bullet variables
var bullet_speed = 400
var bullet_scene = preload("res://bullet.tscn")

enum Rotation_direction {
	STOP = 0,
	RIGHT = 1,
	LEFT = -1
}

func _input(event: InputEvent) -> void:
	# Check for left and right movement
	if Input.is_action_pressed("left"):
		forward_vector.x = -1
	elif Input.is_action_pressed("right"):
		forward_vector.x = 1
	else:
		forward_vector.x = 0

	# Check for spacebar press to shoot bullets
	if event is InputEventKey and event.is_action_pressed("shoot"):
		spawn_bullet()

func spawn_bullet() -> void:
	# Create a new instance of the bullet scene
	var bullet_instance = bullet_scene.instance()

	# Set the bullet's position to the current position of the character
	bullet_instance.position = position

	# Add the bullet as a child of the current scene
	get_parent().add_child(bullet_instance)

	# Calculate the direction in which the bullet should move
	var bullet_direction = Vector2(cos(rotation_angle), sin(rotation_angle))

	# Set the bullet's initial velocity based on the bullet speed and direction
	bullet_instance.velocity = bullet_direction.normalized() * bullet_speed


func _physics_process(delta: float) -> void:
	# Update rotation angle based on rotation speed and direction
	rotation_angle += rotation_speed * rotation_direction

	# Update horizontal movement
	var _horizontal_speed = forward_vector * horizontal_speed
	velocity.x = _horizontal_speed.x

	move_and_slide()

	# Update rotation and vertical position
	self.rotation_degrees = rotation_angle

	# Update position based on velocity
	dpoint = position
	position += velocity * delta
	print(position)

	#keyboard input will cause rotation direction
	if Input.is_action_pressed("rotate_left") == Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.STOP
	elif Input.is_action_pressed("rotate_right"):
		rotation_direction = Rotation_direction.RIGHT
	elif Input.is_action_pressed("rotate_left"):
		rotation_direction = Rotation_direction.LEFT


